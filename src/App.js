import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import axios from "axios";

import NewSchedule from "./components/NewSchedule";
import ExistingSchedule from "./components/ExistingSchedule";
import ScheduleRadio from "./components/ScheduleRadio";
class App extends Component {
    constructor() {
        super();
        this.state =
            {
            showNewScheduleComponent: false,
            showSearchDivComponent: false
            };
        this.showNewSchedule = this.showNewSchedule.bind(this);

    }


   
  render() {
    return (
      <div >
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Pet Appointment Scheduler</h1>
            </header>
            <div className="container">
                <ScheduleRadio parentMethod={this.showNewSchedule.bind(this)} />
                {this.state.showNewScheduleComponent &&
                    <div id="scheduleDiv" className="mainContent">
                        <NewSchedule />
                    </div>}
                {this.state.showSearchDivComponent &&
                    <div id="searchDiv" className="mainContent">
                        <ExistingSchedule />
                    </div>}
            </div>
           
      </div>
    );
    }
    showNewSchedule(props) {
        if (props === 'option1') {
            this.setState({
                showNewScheduleComponent: true,
                showSearchDivComponent: false
            }) 
        }
        else {
            this.setState({
                showNewScheduleComponent: false,
                showSearchDivComponent: true
            })
        }
        
    }
}

export default App;
