﻿import React, { Component } from "react";
import axios from "axios";




class AppTimeLoad extends Component {
    constructor(props) {
        super(props);
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.state = {
            hours: [],
            selectedOption:undefined
        };
    }
    handleOptionChange(changeEvent) {

      
        this.setState({
            selectedOption: changeEvent.target.value
        });

        this.props.parentMethod(changeEvent.target.value);
        //changeEvent.preventDefault();
    }
  
    componentDidMount() {
        var newHours = []
        axios
            .get("http://localhost:9966/petclinic/api/hours")
            .then(response => {
                // create an array of contacts only with relevant data
                newHours = response.data.map(c => {
                    return {
                        id: c.id,
                        name: c.name
                    };
                });
                this.props.parentMethod(newHours[0].id);
                // create a new "state" object without mutating
                // the original state object.
                const newState = Object.assign({}, this.state, {
                    hours: newHours,
                    selectedOption: newHours[0].id
                });

                // store the new state object in the component's state
                this.setState(newState);

            })
            .catch(error => console.log(error));
        
    }

    render() {
        return (
            <div>
            <label>
                Appointment Time:
            </label>
                <select value={this.state.selectedOption} className="drop-down" onChange={this.handleOptionChange.bind(this)}>
                    {this.state.hours.map((c) => <option key={c.id} value={c.id} >{c.name}</option>)}
            </select>
            </div>

        );
    }
}

export default AppTimeLoad;
