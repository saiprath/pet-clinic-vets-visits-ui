﻿import React from 'react';
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';

export default class DayPickerTag extends React.Component {
    constructor(props) {
        super(props);
        this.handleDayClick = this.handleDayClick.bind(this);
        var day = new Date();
        this.state = {
            selectedDay: day,
        };
        this.props.parentMethod(day);
    }
  
    handleDayClick(day, modifiers = {}) {
       
        if (modifiers.disabled) {
            return;
        }
        this.setState({
            selectedDay: modifiers.selected ? undefined : day,
        });
        
        this.props.parentMethod(day);
    }
    render() {
        const today = new Date();
       
        const disabledDays = {
            daysOfWeek: [0, 6],
        };
        return (
            <div>
                <label> 
                    Appointment Date:
                    </label>
                <DayPicker
                    showOutsideDays
                    selectedDays={this.state.selectedDay}
                    disabledDays={[disabledDays , {before: today}]}
                    onDayClick={this.handleDayClick}
                />
                <div>
                    {this.state.selectedDay
                        ? this.state.selectedDay.toLocaleDateString()
                        : 'Please select a day.'}
                </div>
            </div>
        );
    }
}