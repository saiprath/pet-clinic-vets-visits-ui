﻿import React, { Component } from "react";
import axios from "axios";
import "bootstrap/scss/bootstrap.scss";
import JsonTable from "ts-react-json-table";

class ResultsTable extends Component {
    constructor() {
        super();
        this.state =
            {
                

            };
        //this.handleClick = this.handleClick.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    /*handleRemove(changeEvent,hello) {  

        alert("Hello");
        changeEvent.preventDefault();
    }*/
    onSubmit = event => {
      //  event.preventDefault();
        alert("Hello");

        console.log("Param passed => Eth addrs: ", event.target.value)
        console.log("Param passed => id: ", event.target.id)
        event.preventDefault();

        //  ...
    }

   
    handleChange = event => {
        let obj = {};
        obj[event.target.name] = event.target.value;
        this.setState(obj);
    }


    render() {
       var columns = [
            { key: 'ownerFirstName', label: 'Owner First Name' },
            { key: 'ownerLastName', label: 'Owner Last Name' },
            { key: 'petName', label: 'Pet Name' },
            { key: 'vetFirstName', label: 'Vet First Name' },
            { key: 'vetLastName', label: 'Vet Last Name' },
            { key: 'visitDate', label: 'Visit Date' },
            { key: 'hourDesc', label: 'Visit Time' },
            {
                key: 'visitId', label: 'Action', cell: function () {
                    return <button name="visitId" className="btn btn-warning" onClick={this.onSubmit}> Cancel</button>;
                }
            }
        ];
        return (
            
         
            <div className="App">
          
                    <JsonTable rows={this.props.petsForOwnerLastname} columns={columns} theadClassName={'thead-light'} className="table table-sm table-bordered" />
          
            </div>
        );
    }


}

export default ResultsTable;
