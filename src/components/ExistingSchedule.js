﻿import React, { Component } from "react";
import axios from "axios";
import "bootstrap/scss/bootstrap.scss";
import JsonTable from "ts-react-json-table";

class ExistingSchedule extends Component {
    constructor() {
        super();
        this.state =
            {
            petName: '',
            ownerName: '',
            visitId: '',
            cancelButton: '',
            petsForOwnerLastname: [],
          
            columns: [
                { key:'ownerFirstName', label:'Owner First Name' },
                { key: 'ownerLastName', label: 'Owner Last Name' },
                { key: 'petName', label: 'Pet Name' },
                { key: 'vetFirstName', label: 'Vet First Name' },
                { key: 'vetLastName', label: 'Vet Last Name' },
                { key: 'visitDate', label: 'Visit Date' },
                { key: 'hourDesc', label: 'Visit Time' },
                {
                    key: 'visitId', label: 'Action', cell: function (item) {
                        return <button value={item.visitId} className="btn btn-warning" onClick={this.onSubmit}> Cancel</button>;
                    }.bind(this)
                    
                }
            ]
            };
        this.handleClick = this.handleClick.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.makeAxiosCallForVisits = this.makeAxiosCallForVisits.bind(this);
    }

   
    onSubmit = event => {
        axios.delete("http://localhost:9966/petclinic/api/visits/"+event.target.value)
            .then(response => {
                console.log('after delete', response.data);
                this.makeAxiosCallForVisits();
         })
        .catch(function(error) {
          console.log(error);
         });

    }

    handleClick(changeEvent) {
        let ownerNameValid = this.state.ownerName ? true : false;
        let petNameValid = this.state.petName ? true : false;
        let isError = ownerNameValid && petNameValid;
        if (!isError) {
            alert("Enter All Fields");
            changeEvent.preventDefault();
            return false;
        }
        this.makeAxiosCallForVisits();
    }
    makeAxiosCallForVisits() {
        var petnameEntered = this.state.petName;
        
        axios
            .get("http://localhost:9966/petclinic/api/owners/*/lastname/" + this.state.ownerName)

            .then(response1 => {
                // create an array of vet ids only with relevant data
                const newPetsForOwnerLastname = [];
                response1.data.map(e1 => {
                    e1.pets.filter(c => c.name === petnameEntered).map(e2 => {
                        e2.visits.map(e3 => {
                            newPetsForOwnerLastname.push({
                                ownerId: e1.id,
                                ownerFirstName: e1.firstName,
                                ownerLastName: e1.lastName,
                                petId: e2.id,
                                petName: e2.name,
                                vetId: e3.vet.id,
                                vetFirstName: e3.vet.firstName,
                                vetLastName: e3.vet.lastName,
                                hourId: e3.hour.id,
                                hourDesc: e3.hour.name,
                                visitId: e3.id,
                                visitDate: e3.date
                            });
                        });
                    });
                });

                // create a new "state" object without mutating
                // the original state object.
                const newState = Object.assign({}, this.state, {
                    petsForOwnerLastname: newPetsForOwnerLastname
                });

                // store the new state object in the component's state
                this.setState(newState);
            })
            .catch(
                
            
            error => {
                const newState = Object.assign({}, this.state, {
                    petsForOwnerLastname: []
                });

                // store the new state object in the component's state
                this.setState(newState);
                console.log(error);

            });
      //  changeEvent.preventDefault();
    }
    handleChange = event => {
        let obj = {};
        obj[event.target.name] = event.target.value;
        this.setState(obj);
    }


    render() {
        return (
            <div className="App">
            
                

                    <div className="col-sm-6">
                        <div>
                            <label>
                                Pet Name:</label>
                            <input
                                name="petName" value={this.state.petName}
                                type="text" onChange={this.handleChange}
                            /> </div>

                        <label>
                        Owner Last Name:</label>
                    <input
                                name="ownerName" value={this.state.ownerName}
                                type="text" onChange={this.handleChange}
                            />
                        

                    
                    <br /> <br />
                <button className="btn btn-primary" onClick={this.handleClick.bind(this)}>Lookup Schedule</button>
                <br /> <br />
            
                
                </div>

            <JsonTable rows={this.state.petsForOwnerLastname} columns={this.state.columns} theadClassName={'thead-light'} className="table table-sm table-bordered" />
              

                </div>
        );
    }

  
}

export default ExistingSchedule;
