﻿
let var1 = 'firstName'
let value1 = 'Fred'
let var2 = 'lastName'
let value2 = 'Flinstone'

const api = axios.create({ baseURL: 'http://example.com' })
api.post('/user/12345', {
    var1: value1,
    var2: value2
})
    .then(res => {
        console.log(res)
    })
    .catch(error => {
        console.log(error)
    })