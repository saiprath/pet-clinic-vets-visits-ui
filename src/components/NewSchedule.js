﻿import React, { Component } from "react";
import axios from "axios";
import "bootstrap/scss/bootstrap.scss";

import DayPickerTag from "./DatePickerTag";
import AppTimeLoad from "./AppTimeLoad";
import LoadDoctor from "./LoadDoctor";

class NewSchedule extends Component {
    constructor() {
        super();
        this.state =
            {
            selectedDate: null,
            selectedHourid: null,
            selectedVetId: null,
            ownerName: undefined,
            petName: undefined,
            visitReason: undefined,
            petId: null
            };
        this.handleClick = this.handleClick.bind(this);
    }
    handleChange = event => {
        let obj = {};
        obj[event.target.name] = event.target.value;
        this.setState(obj);
    }

    handleClick(changeEvent) {
        //this.validateInputFields();
        let ownerNameValid = this.state.ownerName ? true : false;
        let petNameValid = this.state.petName ? true : false;
        let reasonNameValid = this.state.visitReason ? true : false;
        let isError = ownerNameValid && petNameValid && reasonNameValid;
        if (!isError) {
            alert("Enter All Fields");
            changeEvent.preventDefault();
            return false;
        }

        this.makeAxiosCallForPetId();       
        changeEvent.preventDefault();
       
    }
    insertVisit() {
        var data = this.constructInputForNewAppointment();
        axios
            .post("http://localhost:9966/petclinic/api/visits", data)
            .then(response1 => {
                console.log(response1);
                alert('Successfully added the Appointment');
            })
            .catch(


            error => {
                alert('Appointment not scheduled. Either Vet or Pet has already been scheduled');
                
                console.log(error);

            });

    }

    constructInputForNewAppointment() {
        var day = this.state.selectedDate;
        var dateString = [day.getFullYear(),this.pad(day.getMonth() + 1), this.pad(day.getDate())].join('/');
        var data =
            {
                description: this.state.visitReason,
                pet: { id: this.state.petId.toString() },
                vet: { id: this.state.selectedVetId },
                hour: { id: this.state.selectedHourid },
                date: dateString

            };
        

        return data;
        
    }

    makeAxiosCallForPetId() {
        var petnameEntered = this.state.petName;
        axios
            .get("http://localhost:9966/petclinic/api/owners/*/lastname/" + this.state.ownerName)

            .then(response1 => {
                // create an array of vet ids only with relevant data
                var newPetId = null;
                response1.data.map(e1 => {
                    e1.pets.filter(c => c.name === petnameEntered).map(e2 => {                       
                        newPetId = e2.id;                                                       
                        });
                    });
                
               // store the new state object in the component's state
                this.setState({ petId: newPetId });
                this.insertVisit();
                
               
            }
            )
            .catch(


            error => {
                alert('Pet Not Found, Please create Pet First')
                const newState = Object.assign({}, this.state, {
                    petId: ''
                });

                // store the new state object in the component's state
                this.setState(newState);
                console.log(error);

            });
        //  changeEvent.preventDefault();
    }
    render() {
        return (
            <form>
            <div className="container">
                         
                    <div className="col-sm-6">
                        <div>
                            <label>
                                Pet Name:</label>
                            <input
                                name="petName" value={this.state.petName}
                                type="text" onChange={this.handleChange}
                            /> </div>

                        <label>
                            Owner Last Name:</label>
                        <input
                            name="ownerName" value={this.state.ownerName}
                            type="text" onChange={this.handleChange}
                        />


                    </div>
                    <DayPickerTag parentMethod={this.loadDoctorForDate.bind(this)}/>
                    <AppTimeLoad parentMethod={ this.loadDoctorForHour.bind(this)}/>
                    <LoadDoctor ref="vet" visitDate={this.state.selectedDate} hourID={this.state.selectedHourid} parentMethod={this.getSelectedDoctor.bind(this)}/>
                <div>
                    <label>
                       Reason for the Visit:</label>
                    <input
                            name="visitReason" value={this.state.visitReason}
                            type="text" onChange={this.handleChange}
                    /> </div>
                    <button className="btn btn-primary" onClick={this.handleClick.bind(this)}>Schedule</button>
                </div>
                </form>
        );
    }

    loadDoctorForDate(props) {
        this.setState({
            selectedDate: props
        })  
  
    }

    loadDoctorForHour(props) {
        this.setState({
            selectedHourid: props
        })        
    }

    getSelectedDoctor(props) {
        this.setState({
            selectedVetId: props
        })
    }
    pad(s) { return (s < 10) ? '0' + s : s; };

}

export default NewSchedule;
