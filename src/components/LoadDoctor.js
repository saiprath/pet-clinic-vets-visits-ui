﻿import React, { Component } from "react";
import axios from "axios";
//import LoadVisitVets from "./components/LoadVisitVets";
class LoadDoctor extends Component {
    constructor(props) {
        super(props);
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.state = {
            allVets: [],
            selectedOption: undefined
        };
    }

    handleOptionChange(changeEvent) {

        // alert(changeEvent.target.value);
        this.setState({
          selectedOption: changeEvent.target.value
        });

        this.props.parentMethod(changeEvent.target.value);
        //changeEvent.preventDefault();
    }
    pad(s) { return (s < 10) ? '0' + s : s; };
    componentDidMount() {


        axios
            .get("http://localhost:9966/petclinic/api/vets")
            .then(response => {
                // create an array of contacts only with relevant data
                const newVets = response.data.map(c => {
                    return {
                        id: c.id,
                        name: c.lastName
                    };
                });
                this.props.parentMethod(newVets[0].id);
                // create a new "state" object without mutating
                // the original state object.
                const newState = Object.assign({}, this.state, {
                    allVets: newVets,
                    selectedOption: newVets[0].id
                });

                // store the new state object in the component's state
                this.setState(newState);

            })
            .catch(error => console.log(error));

    }

    render() {


        return (
            <div>
                <label>
                    Select a Doctor:
            </label>
                <select value={this.state.selectedOption} className="drop-down" onChange={this.handleOptionChange.bind(this)}>
                    {this.state.allVets.map((c) => <option key={c.id} value={c.id}>{c.name}</option>)}
                </select>
            </div>

        );
    }
}

export default LoadDoctor;