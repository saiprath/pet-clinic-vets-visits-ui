﻿import React, { Component } from "react";
import "./PetClinic.css";
class ScheduleRadio extends Component{
    constructor() {
        super();
        this.state =
            {
                selectedOption: null
            };
        this.handleOptionChange = this.handleOptionChange.bind(this);
    }
    handleOptionChange(changeEvent) {
        this.setState ( {
            selectedOption: changeEvent.target.value
        })
        this.props.parentMethod(changeEvent.target.value);
    }

    render() {
        return (
            <div className="container">         
                   

                        <form>
                            <div>
                                <span>Please slect an option to go forward</span>
                            </div>
                            <div className="radio">
                                <label>
                                    <input type="radio" value="option1" checked={this.state.selectedOption === 'option1'} onChange={this.handleOptionChange.bind(this)} />
                                    Schedule An Appointment
                </label>
                            </div>
                            <div className="radio">
                                <label>
                                    <input type="radio" value="option2" checked={this.state.selectedOption === 'option2'} onChange={this.handleOptionChange.bind(this)} />
                                    Cancel an appointment
                </label>
                            </div>
                            
                        </form>

                  
                
            </div>
        );
    }
}
export default ScheduleRadio;