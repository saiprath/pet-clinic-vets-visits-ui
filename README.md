Exercise Difficulty: Easy, Moderate, Difficult, Very Difficult

	Moderate

How did you feel about the exercise itself? (1 lowest, 10 highest—awesome way to assess coding ability)

	10

How do you feel about coding an exercise as a step in the interview process?  (1 lowest, 10 highest—awesome way to assess coding ability)

	9

What would you change in the exercise and/or process?

	The assessment is really wonderful. But it took time to understand the table structure of the system 
	and make necessary changes. But overall a great way to assess coding skills.



Please install the backend following the below link before running this ui project

https://gitlab.com/saiprath/pet-clinic-vets-visits-rest



## Running UI locally

This requires git and npm installed already in the machine

```
	git clone https://gitlab.com/saiprath/pet-clinic-vets-visits-ui.git
	cd pet-clinic-vets-visits-ui
	npm install
	npm start
```
Your browser should open a new window for http://localhost:3000

## Notes

Latest version of npm is required to run the UI

App assumes that vets and pets are already existing.


